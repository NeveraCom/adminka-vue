import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './scss/main.scss'
import { createI18n } from '@/plugins/i18n'
import { createPinia } from "pinia";
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import vuetify from './plugins/vuetify'

createApp(App)
  .use(router)
  .use(vuetify)
  .use(createPinia())
  .use(createI18n())
  .mount('#app')

