import type {IUser} from "@/models/interfaces/user.interface";

const url: string = 'https://jsonplaceholder.typicode.com';

export const apiUserService = {
  async getUsers(): Promise<any> {
    const fullUrl = `${url}/users`;
    const posts = await fetch(fullUrl);
    return await posts.json();
  },

  async getUser(userId: number): Promise<any> {
    const fullUrl = `${url}/users/${userId}`;
    const post = await fetch(fullUrl);
    return await post.json();
  },

  async updateUser(user: IUser) {
    const fullUrl = url + '/users/' + user.id;

    const result = await fetch(fullUrl, {
      method: 'PUT',
      body: JSON.stringify(user),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    });
    return await result.json();
  },
}
