import {useMainStore as store} from '@/stores/store';
import {ApiOperation, EntityUrl} from "@/models/enums/api.enum";
import type {IPost} from "@/models/interfaces/post.interface";
import type {IAlbum} from "@/models/interfaces/album.interface";

const url: string = 'https://jsonplaceholder.typicode.com';
type entityType = IPost | IAlbum;

export const apiService = {
  getFullPath(operation: ApiOperation, entityUrl: EntityUrl, id: number = 0, parentEntityUrl: string = ''): string {
    switch (operation) {
      case ApiOperation.delete:
      case ApiOperation.update:
      case ApiOperation.patch:
      case ApiOperation.singleItem:
        return `${url}/${entityUrl}/${id}`;

      case ApiOperation.create:
        return `${url}/${entityUrl}`;

      case ApiOperation.items:
        return `${url}/${parentEntityUrl}/${id}/${entityUrl}`;
    }
  },

  async createItem<T>(item: Partial<T>, entityUrl: EntityUrl): Promise<T> {
    const fullUrl = this.getFullPath(ApiOperation.create, entityUrl); // /posts
    const post = await fetch(fullUrl, {
      method: 'POST',
      body: JSON.stringify(item),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    });
    return await post.json();
  },

  async getItems(entityUrl: EntityUrl, parentEntityId: number, parentEntityUrl: EntityUrl): Promise<any> {
    const fullUrl = this.getFullPath(ApiOperation.items, entityUrl, parentEntityId, parentEntityUrl);
    const posts = await fetch(fullUrl);
    return await posts.json();
  },

  async deleteItem(itemId: number, entityUrl: EntityUrl) {
    const fullUrl = this.getFullPath(ApiOperation.delete, entityUrl, itemId);
    return await fetch(fullUrl, {method: 'DELETE'})
  },

  async getItem(itemId: number, entityUrl: EntityUrl): Promise<any> {
    const fullUrl = this.getFullPath(ApiOperation.singleItem, entityUrl, itemId);
    const post = await fetch(fullUrl);
    return await post.json();
  },

  async updateItem(item: entityType, entityUrl: EntityUrl) {
    const fullUrl = this.getFullPath(ApiOperation.update, entityUrl, item.id);

    const result = await fetch(fullUrl, {
      method: 'PUT',
      body: JSON.stringify(item),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    });
    return await result.json();
  },

  // partial
  async patchItem<T>(id: number, item: Partial<T>, entityUrl: EntityUrl): Promise<any> {
    const fullUrl = this.getFullPath(ApiOperation.patch, entityUrl, id);

    const result = await fetch(fullUrl, {
      method: 'PATCH',
      body: JSON.stringify(item),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    });
    return await result.json();
  }
}
