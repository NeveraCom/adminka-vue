import { defineStore } from "pinia";
import { SnackbarType } from "@/models/enums/snackbar-type.enum";

export const useSnackbarStore = defineStore('snackbarStore', {
  state: () => ({
    message: '',
    isShow: false,
    timing: 2000,
    type: SnackbarType.success
  }),
  actions: {
    show(message: string, type: SnackbarType = SnackbarType.success) {
      this.type = type;
      this.message = message;
      this.isShow = true;
      window.setTimeout(() => this.hide(), this.timing)
    },
    hide() {
      this.isShow = false;
    }
  }
})
