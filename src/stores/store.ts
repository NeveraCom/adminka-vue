import { defineStore } from "pinia";
import type { IUser } from "@/models/interfaces/user.interface";
import { Role as UserRole, type Role } from "@/models/enums/roles.enum";
import { ModeType as modeType, type ModeType } from "@/models/enums/mode-type.enum";
import vuetify from "@/plugins/vuetify";

const url: string = 'https://jsonplaceholder.typicode.com';

export const useMainStore = defineStore('mainStore', {
  state: () => ({
    isLogined: false,
    user: null as (IUser | null),
    role: null as (Role | null),
    darkMode: false,
    isLoading: false
  }),
  actions: {
    async getUsers() {
      const fullUrl = `${url}/users/`;
      const users = await fetch(fullUrl);
      return await users.json();
    },

    setUser(user: IUser): void {
      localStorage.setItem('currentUser', JSON.stringify(user));
      this.user = user;
    },

    setRole(role: Role): void {
      localStorage.setItem('currentUserRole', JSON.stringify(role));
      this.role = role;
    },

    getUserId(): number {
      const value: string | null = localStorage.getItem('currentUser');
      let user: IUser;
      if (value) {
        user = JSON.parse(value);
        return user.id;
      } else {
        return 0;
      }
    },

    getDataFromLocal(): void {
      this.user = JSON.parse(localStorage.getItem('currentUser')!);
      this.role = JSON.parse(localStorage.getItem('currentUserRole')!);
      if ((this.user && this.role === UserRole.user) || this.role === UserRole.admin) {
        this.isLogined = true;
      }
    },

    logout(): void {
      localStorage.clear();
      this.user = null;
      this.role = null;
      this.isLogined = false;
    },

    setLogined(state: boolean = true): void {
      this.isLogined = state;
    },

    toggleDarkMode(): void {
      this.darkMode = !this.darkMode;
      localStorage.setItem('darkMode', JSON.stringify(this.darkMode));
      vuetify.theme.name.value = this.darkMode ? 'dark' : 'light';
    },

    getDarkModeFromLocal(): void {
      this.darkMode = JSON.parse(localStorage.getItem('darkMode') || 'false');
      vuetify.theme.name.value = this.darkMode ? 'dark' : 'light';
    },

    getMode(userId: number): ModeType {
      const isOwner: boolean = this.user?.id === +userId;
      const isAdmin: boolean = this.role === UserRole.admin;
      return isOwner || isAdmin ? modeType.edit : modeType.view;
    }
  }
});
