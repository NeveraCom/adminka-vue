import {defineStore} from "pinia";
import type {IDialogData} from "@/models/interfaces/dialog-data.interface";
import type {IPost} from "@/models/interfaces/post.interface";
import type {IAlbum} from "@/models/interfaces/album.interface";
import type {ITodo} from "@/models/interfaces/todo.interface";
import {EntityModalType} from "@/models/enums/entity-modal-types.enum";
import type {IPhoto} from "@/models/interfaces/photo.interface";
import {apiService} from "@/services/api/api-service";
import {EntityUrl} from "@/models/enums/api.enum";

export const useEntityDialogStore = defineStore('entityDialogStore', {
  state: () => ({
    show: false as boolean,
    title: '' as string,
    message: '' as string,
    approveButtonCaption: '' as string,
    declineButtonCaption: '' as string,

    post: {} as IPost,
    album: {} as IAlbum,
    todo: {} as ITodo,
    photo: {} as IPhoto,
    entityType: EntityModalType.post as EntityModalType
  }),

  getters: {
    isShow(): boolean {
      return this.show;
    },
    getTitle(): string {
      return this.post?.title || this.album?.title || this.todo?.title || '';
    },
  },

  actions: {
    resolve: (entity: ITodo | IPost | IAlbum | IPhoto | boolean) => {},

    reject: (answer: boolean) => {},

    hideModal() {
      this.show = false;
    },

    async showModal<T>({
          title,
          message = '',
          approveButtonCaption,
          declineButtonCaption,
          post = {} as IPost,
          album = {} as IAlbum,
          todo = {} as ITodo,
          photo = {} as IPhoto,
          entityType
        }: IDialogData): Promise<T> {
      this.title = title;
      this.message = message;
      this.approveButtonCaption = approveButtonCaption;
      this.declineButtonCaption = declineButtonCaption;
      this.post = post;
      this.album = album;
      this.todo = todo;
      this.photo = photo;
      this.entityType = entityType;

      this.show = true;

      return new Promise((resolve, reject): void => {
        this.resolve = resolve;
        this.reject = reject;
      });
    },

    agree(entity: ITodo | IPost | IAlbum | IPhoto | boolean): void {
      this.resolve(entity);
      this.show = false;
    },

    cancel(): void {
      this.resolve(false);
      this.show = false;
    },

    getPost(title: string, body: string, userId: number, post: IPost): Promise<IPost> {
      if (post.id) {
        const editedPost = Object.assign(post, { title, body })
        return apiService.updateItem(editedPost, EntityUrl.posts)

      } else {
        const newPost: IPost = {
          id: Date.now(),
          title,
          body,
          userId
        }
        return apiService.createItem<IPost>(newPost, EntityUrl.posts)
      }
    },

    getTodo(title: string, userId: number, todo: ITodo): Promise<ITodo> {
      if (todo.id) {
        const editedTodo = Object.assign(todo, { title })
        return apiService.updateItem(editedTodo, EntityUrl.posts)

      } else {
        const newTodo: ITodo = {
          id: Date.now(),
          title,
          completed: false,
          userId
        }
        return apiService.createItem<ITodo>(newTodo, EntityUrl.todos)
      }
    },

    getAlbum(title: string, userId: number, album: IAlbum): Promise<IAlbum> {
      if (album.id) {
        const editedAlbum = Object.assign(album, { title })
        return apiService.updateItem(editedAlbum, EntityUrl.albums)

      } else {
        const newAlbum: IAlbum = {
          id: Date.now(),
          title,
          userId
        }
        return apiService.createItem<IAlbum>(newAlbum, EntityUrl.albums)
      }
    },

    getPhoto(title: string, url: string, thumbnailUrl: string, albumId: number): Promise<IPhoto> {
      const newPhoto: IPhoto = {
        title,
        url,
        thumbnailUrl,
        id: Date.now(),
        albumId
      }
      return apiService.createItem<IPhoto>(newPhoto, EntityUrl.photos)
    },

  }
})
