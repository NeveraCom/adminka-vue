import { defineStore } from "pinia";
import { LanguagesEnum as Langs, type LanguagesEnum } from "@/models/enums/languages.enum";

export const useLangStore = defineStore('langStore', {
  state: () => ({
    currentLang: Langs.en as LanguagesEnum,
  }),
  getters: {
    currentLangName(): string {
      return 'LANG.' + this.currentLang.toUpperCase();
    },
  },
  actions: {
    changeLang(lang: LanguagesEnum): void {
      this.currentLang = lang;
      this.saveCurrentLang(lang);
    },

    saveCurrentLang(lang: LanguagesEnum): void {
      localStorage.setItem('currentLang', JSON.stringify(this.currentLang));
    },

    loadCurrentLang(): void {
      const lang = JSON.parse(localStorage.getItem('currentLang') as string);
      if (lang) {
        this.currentLang = lang;
      }
    },

    getLangList(): string[] {
      return Object.keys(Langs).filter((lang: string): boolean => lang !== this.currentLang)
    }
  }
})
