import {defineStore} from "pinia";
import {MessageModalType} from "@/models/enums/message-modal-type.enum";
import type {ICommonDialogData} from "@/models/interfaces/dialog-data.interface";

export const useMessageDialogStore = defineStore('messageDialogStore', {
  state: () => ({
    show: false as boolean,
    title: '' as string,
    message: '' as string,
    approveButtonCaption: '' as string,
    declineButtonCaption: '' as string,
    modalType: MessageModalType.alert as MessageModalType,
  }),

  getters: {
    isShow(): boolean {
      return this.show;
    }
  },

  actions: {
    resolve: (answer: boolean) => {},
    reject: (answer: boolean) => {},

    hideModal() {
      this.show = false;
    },

    async showModal({
           title,
           message = '',
           approveButtonCaption,
           declineButtonCaption = '',
           modalType = MessageModalType.alert
         }: ICommonDialogData) {
      this.title = title;
      this.message = message;
      this.approveButtonCaption = approveButtonCaption;
      this.declineButtonCaption = declineButtonCaption;
      this.modalType = modalType;

      this.show = true;

      return new Promise((resolve, reject): void => {
        this.resolve = resolve;
        this.reject = reject;
      });
    },
    agree() {
      this.resolve(true);
      this.show = false;
    },
    cancel() {
      this.resolve(false);
      this.show = false;
    }
  }
})
