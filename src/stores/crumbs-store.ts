import {defineStore} from "pinia";
import type {ICrumb} from "@/models/interfaces/crumb.interface";

export const useCrumbsStore = defineStore('crumbsStore', {
  state: () => ({
    crumbs: [] as ICrumb[],
    maxLengthTitle: 40
  }),
  getters: {
    getCrumbs(): ICrumb[] {
      return this.crumbs;
    }
  },
  actions: {
    setCrumbs(crumbs: ICrumb[]) {
      this.crumbs = crumbs
        .map(e => e.title?.length > this.maxLengthTitle
          ? Object.assign(e, {title: e.title.slice(0, this.maxLengthTitle) + '...'})
          : e)
    }
  }
});
