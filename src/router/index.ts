import { createRouter, createWebHistory } from 'vue-router'
import Posts from '@/pages/posts.vue';
import Post from '@/pages/post.vue';
import Albums from '@/pages/albums.vue';
import Statistics from '@/pages/statistics.vue';
import Todos from '@/pages/todos.vue';
import UserData from '@/pages/user-data.vue';
import Users from '@/pages/users/users.vue';
import User from '@/pages/user.vue';
import NotFoundPage from '@/pages/not-found-page.vue';
import posts from "@/pages/posts.vue";
import Album from "@/pages/album.vue";

const router = createRouter({
  // history: createWebHistory(import.meta.env.BASE_URL),
  history: createWebHistory(),
  routes: [
    {
      path: '/users',
      component: Users
    },
    {
      path: '/users/:userId',
      component: User,
      children: [
        {
          path: '/users/:userId',
          redirect: to => {
            return { path: to.fullPath + '/user-data' }
          }
        },
        {
          path: 'user-data',
          component: UserData
        },
        {
          path: 'posts',
          component: Posts
        },
        {
          path: 'posts/:id',
          component: Post
        },
        {
          path: 'albums',
          component: Albums
        },
        {
          path: 'albums/:id',
          component: Album
        },
        {
          path: 'todos',
          component: Todos
        },
      ]
    },
    {
      path: '/user-data',
      component: UserData
    },
    {
      path: '/posts',
      component: Posts
    },
    {
      path: '/posts/:id',
      component: Post
    },
    {
      path: '/albums',
      component: Albums
    },
    {
      path: '/albums/:id',
      component: Album
    },
    {
      path: '/todos',
      component: Todos
    },
    {
      path: '/statistics',
      component: Statistics
    },
    {
      path: '',
      redirect: '/users'
    },
    {
      path: '/:pathMatch(.*)*',
      component: NotFoundPage
    }
  ]
})

export default router
