import { createI18n as createVueI18n } from 'vue-i18n'
import englishLang from './../assets/i18n/en';
import ukrainianLang from './../assets/i18n/ua';
import { useLangStore } from "@/stores/lang-store";

const messages = {
  en: englishLang,
  ua: ukrainianLang
}

export const createI18n = () =>
  createVueI18n({
    locale: useLangStore().currentLang,
    messages
  })
