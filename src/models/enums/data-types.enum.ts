export enum DataType {
  post = 'post',
  album = 'album',
  todo = 'todo',
}