export enum ApiOperation {
  delete = 'delete',
  create = 'create',
  update = 'update',
  singleItem = 'singleItem',
  items = 'items',
  patch = 'patch'
}

export enum EntityUrl {
  posts = 'posts',
  post = 'post',
  todos = 'todos',
  comments = 'comments',
  users = 'users',
  albums = 'albums',
  photos = 'photos'
}
