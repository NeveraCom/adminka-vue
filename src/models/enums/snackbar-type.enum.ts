export enum SnackbarType {
  error = 'error',
  success = 'success'
}
