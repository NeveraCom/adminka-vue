export interface ICrumb {
  title: string,
  disabled: boolean,
  to: string,
  notTranslate?: boolean
}
