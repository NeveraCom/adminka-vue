import type { Role } from "@/models/enums/roles.enum";

export interface INavItem {
  icon: string;
  title: string;
  link: string;
  roles: Role[];
}
