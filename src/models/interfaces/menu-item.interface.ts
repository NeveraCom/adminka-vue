export interface IMenuItem {
  icon: string;
  title: string;
  iconColor: string;
  value: string;
}
