import type { EntityModalType } from "@/models/enums/entity-modal-types.enum";
import type { IPost } from "@/models/interfaces/post.interface";
import type { IAlbum } from "@/models/interfaces/album.interface";
import type { ITodo } from "@/models/interfaces/todo.interface";
import type { MessageModalType } from "@/models/enums/message-modal-type.enum";
import type { IPhoto } from "@/models/interfaces/photo.interface";

export interface ICommonDialogData {
  title: string;
  message?: string;
  modalType?: MessageModalType;
  approveButtonCaption: string;
  declineButtonCaption?: string;
}

export interface IDialogData extends ICommonDialogData {
  entityType: EntityModalType;
  declineButtonCaption: string;
  post?: IPost;
  album?: IAlbum;
  todo?: ITodo;
  photo?: IPhoto;
  albumId?: number;
  PhotoId?: number;
}
