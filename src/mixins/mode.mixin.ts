import {ModeType} from "@/models/enums/mode-type.enum";
import type {ICrumb} from "@/models/interfaces/crumb.interface";
import {useCrumbsStore} from "@/stores/crumbs-store";
import type {IUser} from "@/models/interfaces/user.interface";

const modeMixin = {
  data() {
    return {
      userId: 0 as number,
      user: {} as IUser,
      baseBreadCrumbs: [] as ICrumb[],
      fullBreadCrumbs: true,
      mode: ModeType.view,
      crumb: useCrumbsStore(),
    }
  },
  computed: {
    getBaseCrumbsList(): ICrumb[] {
      let crumbs: ICrumb[] = [];
      if (this.fullBreadCrumbs) {
        crumbs.push({
          title: 'BREAD_CRUMBS.USERS',
          disabled: false,
          to: '/users'
        });
        crumbs.push({
          title: this.user?.name,
          disabled: false,
          notTranslate: true,
          to: `/users/${this.userId}`
        });
      }
      return crumbs;
    }
  },
  methods: {
    defineParams(): void {
      this.userId = this.$route?.params['userId'];

      if (!this.userId) {
        this.userId = this.store.getUserId();
        this.fullBreadCrumbs = false
      }
      this.mode = this.store.getMode(this.userId);
    },
  },
  mounted() {
    this.defineParams();
  }
}

export default modeMixin;
