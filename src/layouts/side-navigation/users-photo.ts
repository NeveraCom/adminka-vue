import img1 from '@/assets/images/users-photo/1.jpg';
import img2 from '@/assets/images/users-photo/2.jpg';
import img3 from '@/assets/images/users-photo/3.jpg';
import img4 from '@/assets/images/users-photo/4.jpg';
import img5 from '@/assets/images/users-photo/5.jpg';
import img6 from '@/assets/images/users-photo/6.jpg';
import img7 from '@/assets/images/users-photo/7.jpg';
import img8 from '@/assets/images/users-photo/8.jpg';
import img9 from '@/assets/images/users-photo/9.jpg';
import img10 from '@/assets/images/users-photo/10.jpg';
import unknownImg from '@/assets/images/users-photo/unknown.jpg';
import adminImg from '@/assets/images/users-photo/admin.jpg';

export default [
  img1, img2, img3, img4, img5, img6, img7, img8, img9, img10,
];

export const adminPhoto: string = adminImg;
export const unknownPhoto: string = unknownImg;
